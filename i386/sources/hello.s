.text

#	I allocate to the variable `message` a region of memory containing the
#	message "Hello, World!" (without the surrounding double quotes) as an ASCII
#	string, and allocate to the variable `newline` a region of memory
#	containing a single ASCII newline character.  Since I make the allocation
#	to `message` and then make the allocation to `newline` without making any
#	interceding allocations, the last byte of the region allocated to `message`
#	must immediately preceed the first byte of the region allocated to
#	`newline`.  Therefore
#
#	-   the concatenation of the two contiguous regions contains the message
#	    with a trailing newline character,
#
#	-   the difference between the addresses of the regions allocated to
#	    `newline` and `message` gives the number of bytes in the region
#	    containing the message without a trailing newline character, and
#
#	-   adding one to this difference gives the number of bytes in the region
#	    containing the message with a trailing newline character.

message:
	.ascii "Hello, World!"

newline:
	.ascii "\n"

.type _start, @function
.global _start
_start:

	#	I first write the memory addresses of `message` and `newline` to the
	#	registers `ecx` and `edx` respectively.

	mov $message, %ecx
	mov $newline, %edx

	#	I then calculate the number of bytes in the memory region containing
	#	the message with a trailing newline character, and write the result to
	#	`edx`.

	sub %ecx, %edx
	inc %edx

	#	I write 1, the file descriptor for the standard input, to register
	#	`ebx`.

	mov $1, %ebx

	#	I make the system call `write %ebx %ecx %edx`, where
	#
	#	-   `ebx` contains the file descriptor for the standard output,
	#	
	#	-   `ecx` contains ihe address of the memory region containing the
	#	    message, and
	#
	#	-  	`edx` contains the number of bytes in the memory region containing
	#	    the message with a trailing newline character.
	#
	#	This writes all of the message with a trailing newline character to the
	#	standard output.

	mov $4, %eax
	int $128

	# I make the system call `exit 0`.  This exits with status `0`.

	mov $0, %ebx
	mov $1, %eax
	int $128

