.text
.global _write_register
.type _write_register, @function
_write_register:

	# This subroutine tries to follow the CDECL calling convention, and I
	# document it extensively for my own future reference.

	# Note: I am running on an AMD64 processor.  An AMD64 processor cannot push
	# or pop the content of a 32 bit register to or from the stack, and the
	# base and stack pointer registers contain 64 bit addresses.  I therefore
	# use the 64 bit `r` registers that subsume the 32 bit `e` registers when I
	# push to or pop from the stack and when I work with the base or stack
	# pointers.  Everywhere else, I use the 32 bit `e` registers.

	# CDECL Prologue
	# ==============

	# Prior to the caller calling me, the stack and the content of the stack
	# pointer register `esp` look like

	#     ├────────────────────────────────────────────────┤◁ stack pointer
	#     │ <previous item>                                │
	#     │                                                │
	#     ├────────────────────────────────────────────────┤

	# I take three parameters
	#
	# -   the content of a register that I write to memory as a sequence of
	# ASCII characters,
	#
	# -   the address of a region of memory that I write the sequence of ASCII
	# characters to, and
	#
	# -   the length of the region of memory that I write the sequence of ASCII
	# characters to (and cannot exceed).
	#
	# The caller pushes these parameters (in reverse order) to the stack before
	# issuing the `call` instruction, which itself pushes the return address of
	# the caller to the stack.  Thus, when I start, the stack and the content
	# of the stack pointer register `esp` look like

	#     ├────────────────────────────────────────────────┤◁ stack pointer
	#     │ caller's return address                        │
	#     │                                                │
	#     ├────────────────────────────────────────────────┤
	#     │ first parameter: the content of a register     │
	#     │ that I write to memory as an ASCII sequence    │
	#     ├────────────────────────────────────────────────┤
	#     │ second parameter: the address of a region of   │
	#     │ memory that I write the ASCII sequence to      │
	#     ├────────────────────────────────────────────────┤
	#     │ third parameter: the length of the region of   │
	#     │ memory that I write the ASCII sequence to      │
	#     ├────────────────────────────────────────────────┤
	#     │ <previous item>                                │
	#     │                                                │
	#     ├────────────────────────────────────────────────┤

	# Base Pointer
	# ------------

	# I don't know the content of the base pointer register `ebp`, but the
	# caller expects that content to be unchanged when I return.  Since I use
	# the base pointer register as a reference point when accessing the stack,
	# I push the current content of the base pointer register to the stack so
	# that I can restore its content when I finish, and write the current
	# content of the stack pointer register to the base pointer register.

	push %rbp
	mov %rsp, %rbp

	# The stack and the contents of the base pointer register `ebp` and the
	# stack pointer register `esp` now look like

	#     ├────────────────────────────────────────────────┤◁ stack pointer
	#     │ content of base pointer register before call   │  base pointer
	#     │                                                │
	#     ├────────────────────────────────────────────────┤
	#     │ caller's return address                        │
	#     │                                                │
	#     ├────────────────────────────────────────────────┤
	#     │ first parameter: the content of a register     │
	#     │ that I write to memory as an ASCII sequence    │
	#     ├────────────────────────────────────────────────┤
	#     │ second parameter: the address of a region of   │
	#     │ memory that I write the ASCII sequence to      │
	#     ├────────────────────────────────────────────────┤
	#     │ third parameter: the length of the region of   │
	#     │ memory that I write the ASCII sequence to      │
	#     ├────────────────────────────────────────────────┤
	#     │ <previous item>                                │
	#     │                                                │
	#     ├────────────────────────────────────────────────┤

	# Callee Saved Registers
	# ----------------------

	# The registers `eax`, `ecx` and `edx` are *caller saved* registers: the
	# caller *cannot* expect the content of each of these registers to be the
	# same when I return as it was when I was called.  I am allowed to change
	# and not restore the content of each of these registers.  If the caller
	# requires the content of any of these registers to survive calling me, it
	# is the caller's responsibility to save its content elsewhere.  On the
	# other hand, the registers `ebx`, `edi` and `esi` are *callee saved*: the
	# caller *can* expect the content of each of these registers to be the same
	# when I return as it was when I was called.  If I change the content of
	# any of these registers, it is my responsibility to restore its content
	# before I return.  I use all three registers, so I push each to the stack
	# now, to be restored just before I return.

	push %rbx
	push %rdi
	push %rsi

	# The stack and the content of the base and stack pointer registers now
	# look like

	#     ├────────────────────────────────────────────────┤◁ stack pointer
	#     │ content of register `rsi`                      │
	#     │                                                │
	#     ├────────────────────────────────────────────────┤
	#     │ content of register `rdi`                      │
	#     │                                                │
	#     ├────────────────────────────────────────────────┤
	#     │ content of register `rbx`                      │
	#     │                                                │
	#     ├────────────────────────────────────────────────┤◁ base pointer
	#     │ content of base pointer register before call   │
	#     │                                                │
	#     ├────────────────────────────────────────────────┤
	#     │ caller's return address                        │
	#     │                                                │
	#     ├────────────────────────────────────────────────┤
	#     │ first parameter: the content of a register     │
	#     │ that I write to memory as an ASCII sequence    │
	#     ├────────────────────────────────────────────────┤
	#     │ second parameter: the address of a region of   │
	#     │ memory that I write the ASCII sequence to      │
	#     ├────────────────────────────────────────────────┤
	#     │ third parameter: the length of the region of   │
	#     │ memory that I write the ASCII sequence to      │
	#     ├────────────────────────────────────────────────┤
	#     │ <previous item>                                │
	#     │                                                │
	#     ├────────────────────────────────────────────────┤

	# Parameters
	# ----------

	# The AMD64 processor has 64 bit (8 byte) stack items, so copies of the
	# caller's first, second and third paratmeters are held on the stack at
	# addresses 16, 24 and 32 bytes beyond the content of the base pointer
	# register:

	#     ├────────────────────────────────────────────────┤◁ base pointer
	#     │ content of base pointer register before call   │
	#     │                                                │
	#     ├────────────────────────────────────────────────┤◁ base pointer + 8
	#     │ caller's return address                        │
	#     │                                                │
	#     ├────────────────────────────────────────────────┤◁ base pointer + 16
	#     │ first parameter: the content of a register     │
	#     │ that I write to memory as an ASCII sequence    │
	#     ├────────────────────────────────────────────────┤◁ base pointer + 24
	#     │ second parameter: the address of a region of   │
	#     │ memory that I write the ASCII sequence to      │
	#     ├────────────────────────────────────────────────┤◁ base pointer + 32
	#     │ third parameter: the length of the region of   │
	#     │ memory that I write the ASCII sequence to      │
	#     ├────────────────────────────────────────────────┤

	# I copy the parameters at addresses 16, 24 and 32 bytes beyond the content
	# of the base pointer register to the registers `eax`, `edi` and `esi`
	# respectively.

	mov 16(%rbp), %eax
	mov 24(%rbp), %edi
	mov 32(%rbp), %esi

	# Thus,
	#
	# -   `eax` contains the content of a register that I write to memory as an
	# ASCII sequence,
	#
	# -   `edi` contains the address of a region of memory that I write the
	# ASCII sequence to, and 
	#
	# -    `esi` contains the length of the region of memory that I write the
	# ASCII sequence to.

	# Body
	# ====

	mov %esi, %ecx

	# I use the number in register `rcx` as an offset to traverse through the
	# memory at the address in register `rbx from high to low.  I set the
	# number in `rcx` to the highest possible offset by decrementing its
	# current value, the length of the memory indicated by `rbx`.

	dec %ecx

	# I write the null character at the highest offset of the memory allocated
	# to.

	movb $0, (%edi,%ecx)

	# I start a loop in which I write characters to the rest of the memory
	# allocated to `pid` from highest offset to lowest.  Starting from the
	# subroutine parameter `«num»`, I repeatedly divide `«num»` by `10`,
	# writing the moduli to the string from the right until either the memory
	# is full or `«num»` has been reduced to `0`.

	begin1:

		# If the offset is zero then I have filled the memory.  I exit this
		# loop.

		cmp $0, %ecx
		jl end1

		dec %ecx

		# On the 64-bit Intel/AMD architevture, the instruction
		#
		#     div %«reg»
		#
		# divides the 128 bit number whose high 64 bits are in register `rdx`
		# and low 64 bits are in register `rax` by the value of register
		# `«reg»`, and writes the quotient to `rax` and the remainder to `rdx`.
		# I write decimal 0 to `rdx`, write decimal `10` to register `xxx`, and
		# divide by `xxx`.  Therefore, if the content of `rax` is `«num»`, then
		# I wrote
		#
		#     «num»/10
		#
		# to `rax` and
		#
		#     «num»%10
		#
		# to `rdx`.

		mov $0, %edx
		mov $10, %ebx
		div %ebx

		# I write `«num»%10 + 48` to the current offset, and decrement the
		# offset.  The addition of `48` converts the byte `«num»%` to the ASCII
		# code for the corresponding decimal digit.

		movb %dl, (%edi,%ecx)
		add $48, (%edi,%ecx)

		# If `«num»/10` is not `0` then I have converted the entirety of the
		# subroutine parameter to ASCII decimal digits, so I repeat the loop
		# with `«num»/10` in place of `«num»`,...

		cmp $0, %eax
		jne begin1
	end1:

	mov %esi, %eax
	sub %ecx, %eax  # This sets eax to its final value.

	mov $0, %edx
	begin2:
		mov (%edi,%ecx), %ebx
		mov %ebx, (%edi,%edx)	

		inc %ecx
		inc %edx

		cmp %esi, %ecx
		jne begin2

	begin3:
		movb $0, (%edi,%edx)

		inc %edx

		cmp %esi, %edx
		jne begin3

	# CDECL Epilogue
	# ==============

	pop %rsi
	pop %rdi
	pop %rbx

	mov %rbp, %rsp
	pop %rbp

	ret
