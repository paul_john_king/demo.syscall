.text
.global _start
_start:

	#	I make the system call `exit 59`.  This exits with status `59`.

	mov $59, %ebx
	mov $1, %eax
	int $128
