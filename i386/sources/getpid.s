.text
.global _start
.type _start, @function
_start:

	#	I make the system call `getpid`.  If successful, this writes my PID to
	#	the register `eax`.

	mov $20, %eax
	int $128

	#	I allocate 21 bytes of memory to the variable `pid`, write the address
	#	of the allocated memory to the register `ebx`, and write the quantity
	#	of the allocated memory to the register `ecx`.

	.lcomm pid, 21
	lea pid, %ebx
	mov $21, %ecx

	#	I call the subroutine `_write_register rax rbx rcx` using the CDECL
	#	calling convention, where
	#
	#		`rax` contains my PID,
	#
	#		`rbx` contains the address of the allocated memory, and
	#
	#		`rcx` contains the quantity of the allocated memory.

	#	Note 1: I am running on an AMD64 processor, and an AMD64 processor
	#	cannot push 32 bit registers on to the stack.  I therefore push the 64
	#	bit registers `rax`, `rbx` and `rcx` that subsume the 32 bit `eax`,
	#	`ebx` and `ecx` respectively.

	#	Note 2: `ebx` is callee saved under the CDECL calling convention, that
	#	is, the callee should ensure that the content of the register is the
	#	same when the callee returns as it was when the callee was called.
	#	Therefore, `ebx` should still hold the address of the allocated memory
	#	after the subroutine returns.  `eax`and `ecx`, on the other hand, are
	#	caller saved under the CDECL calling convention, that is, the callee
	#	may change their content if it so wishes.  The caller must save their
	#	content elsewhere if it needs to be sure that they survive the
	#	subroutine call.  Luckily, we do not need to preserve the contents of
	#	`eax` or `ecx`.

	#	The subroutine call writes up to 21 (the content of `ecx`) of the
	#	trailing bytes of a string containing a null-terminated decimal
	#	representation of my PID (the content of `eax`) to memory allocated at
	#	a given address (the content of `ebx`).  It also writes the number of
	#	bytes actually written to `eax`.

	push %rcx
	push %rbx
	push %rax
	call _write_register
	add $24, %esp

	#	I replace the trailing null character of the string with a trailing
	#	newline character (ASCCI code decimal 10).

	movb $10, -1(%ebx,%eax)

	#	I make the system call `write ebx ecx edx` where
	#
	#		`ebx` contains `1` (the file descriptor of the standard output),
	#
	#		`ecx` contains the address of the string, and
	#
	#		`edx` contains the length of the string (including the trailing
	#		newline character).
	#
	#	This writes exactly the bytes of the string (including the trailing
	#	newline character) to the standard output.

	mov %eax, %edx
	mov %ebx, %ecx
	mov $1, %ebx
	mov $4, %eax
	int $128

	#	I make the system call `exit ebx`, where
	#
	#		`ebx` contains `0`.
	#
	#	This exits with status `0`.

	mov $0, %ebx
	mov $1, %eax
	int $128
