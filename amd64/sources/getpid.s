.text
.global _start
_start:

	#	I make the system call `getpid`.  This writes my PID to register `rax`.
	#	I copy this to register `r8`.

	mov $39, %rax
	syscall
	mov %rax, %r8

	#	I allocate 21 bytes of memory to variable `pid`, write the address of
	#	the allocated memory to register `r9`, and write the quantity of the
	#	allocated memory to register `r10`.

	.lcomm pid, 21
	lea pid, %r9
	mov $21, %r10

	#	I call subroutine `write_register r8 r9 r10`.  This writes up to 21
	#	(the value of register `r10`) of the trailing bytes of a string
	#	containing a null-terminated decimal representation of my PID (the
	#	value of register `r8`) to memory at a given address (the value of
	#	register `r9`).  It also writes the number of bytes actually 

	#	the variable `pid` (whose address in
	#	indicated by register `rbx`) and the length of that string to the
	#	register `rax`.

	push %r10
	push %r9
	push %r8
	call write_register
	mov %rax, %r8
	add $24, %rsp

	#	I replace the trailing null character with a trailing newline
	#	character.

	movb $10,-1(%r9,%r8)

	#	I make the system call `write 1 rbx rax`.  This writes exactly the
	#	bytes of the string indicated by the `rbx` register (a string
	#	containing a newline-terminated decimal representation of my PID) to
	#	the standard output.

	mov %r8,%rdx
	mov %r9,%rsi
	mov $1,%rdi
	mov $1,%rax
	syscall

	#	I make the system call `exit 0`.  This exits with status `0`.

	mov $0,%rdi
	mov $60,%rax
	syscall
