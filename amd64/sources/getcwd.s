.text
.global _start
.type _start, @function
_start:

	#	I allocate 4097 bytes of heap memory to the variable `cwd`.

	.lcomm cwd 4097

	#	I make the system call `getcwd cwd 4096`.  If successful, this writes
	#	up to 4096 bytes of my current working directory (without a trailing
	#	newline character) to the variable `cwd`, and writes the number of
	#	bytes written to the register `rax`.

	mov $4096, %rsi
	mov $cwd, %rdi
	mov $79, %rax
	syscall

	#	I write the 1 byte newline character (ASCII code decimal 10) to the
	#	variable `cwd` immediately after the last byte of my current working
	#	directory, and increment the register `rax` to include the newly
	#	written character.

	movb $10, cwd(,%rax,1)
	inc %rax

	#	I make the system call `write 1 cwd rax`.  This writes the first `rax`
	#	bytes of variable `cwd` to the standard output, where
	#
	#	-   variable `cwd` contains the bytes of my current working directory
	#	    (with a trailing newline character), and
	#
	#	-   register `rax` contains the number of bytes of my current working
	#	    directory (with a trailing newline character).
	#
	#	In short, this writes my newline-terminated current working directory
	#	to the standard output.

	mov %rax, %rdx
	mov $cwd, %rsi
	mov $1, %rdi
	mov $1, %rax
	syscall

	#	I make the system call `exit 0`.  This exits with status `0`.

	mov $0, %rdi
	mov $60, %rax
	syscall
