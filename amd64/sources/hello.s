.text

#	I assign an ASCII string comprising my message without a trailing newline
#	character to the variable `message`, and assign an ASCII string comprising
#	a newline character to the immediately following variable `message_end`.  I
#	later use the difference between the effective addresses of these variables
#	to calculate the length of the message.

message:
	.ascii "Hello, World!"
message_end:
	.ascii "\n"

.type _start, @function
.global _start
_start:

	#	I calculate the length of my message with a trailing newline character
	#	by
	#
	#	-   subtracting the effective address of the variable `message` from
	#	    the effective address of the variable `message_end`, and
	#
	#	-   adding 1 to the result.
	#
	#	Since the variable `message_end` immediately succeeds the variable
	#	`message` in memory, the difference between the effective addresses
	#	gives the number of bytes in my message without a trailing newline
	#	character.  Adding one to the difference then gives the number of bytes
	#	in my message with a trailing newline character.  I write the length to
	#	register `rdx`.

	lea message, %rcx
	lea message_end, %rdx
	sub %rcx, %rdx
	inc %rdx

	#	I make the system call `write 1 message rdx`.  Since
	#
	#	-  	register `rdx` contains 1 plus the number of bytes in the variable
	#	    `message`,
	#
	#	-   variable `message_end` immediately succeeds variable `message` in
	#	    memory,
	#
	#	-   variable `message` contains my message without a trailing newline
	#	    character, and
	#
	#	-   variable `message_end` contains only a newline character,
	#
	#	this writes my message with a trailing newline chaacter to the standard
	#	output.

	mov $message, %rsi
	mov $1, %rdi
	mov $1, %rax
	syscall

	# I make the system call `exit 0`.  This exits with status `0`.

	mov $0, %rdi
	mov $60, %rax
	syscall

