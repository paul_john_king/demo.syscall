.text
.global write_register
.type write_register, @function
write_register:

	#	I am trying to follow the C calling conventions in this subroutine, and
	#	will document what I am doing extensively for my own future reference.
	#	I am drawing heavily from
	#	`http://flint.cs.yale.edu/cs421/papers/x86-asm/asm.html`.

	#	Prologue
	#	========

	#	Caller Parameters
	#	-----------------

	#	Prior to the caller calling me, the stack and the value of the stack
	#	pointer register look like

	#		┌────────────────────────────────┐
	#		│ <previous item>                │ < stack pointer
	#		├────────────────────────────────┤

	#	Since I take three parameters, the caller pushes three parameters (in
	#	reverse order) to the stack before issuing the `call` instruction,
	#	which itself pushes the return address of the caller to the stack.  The
	#	stack and the value of the stack pointer register now look like

	#		┌────────────────────────────────┐
	#		│ caller's return address        │ < stack pointer
	#		├────────────────────────────────┤
	#		│ caller's first parameter       │
	#		├────────────────────────────────┤
	#		│ caller's second parameter      │
	#		├────────────────────────────────┤
	#		│ caller's third parameter       │
	#		├────────────────────────────────┤
	#		│ <previous item>                │
	#		├────────────────────────────────┤

	#	Base Pointer Register
	#	---------------------

	#	The base pointer register has an unknown value that the caller expects
	#	to be unchanged when I have finished, but I use the base pointer
	#	register as a referece point when accessing the stack.  I therefore
	#	push the current value of the base pointer register to the stack so
	#	that I can restore its value when I finish, and write the current value
	#	of the stack pointer register to the base pointer register.

	push %rbp
	mov %rsp, %rbp

	#	The stack and the value of the base and stack pointer registers now
	#	look like

	#		┌────────────────────────────────┐
	#		│ base pointer before call       │ < stack pointer, base pointer
	#		├────────────────────────────────┤
	#		│ caller's return address        │
	#		├────────────────────────────────┤
	#		│ caller's first parameter       │
	#		├────────────────────────────────┤
	#		│ caller's second parameter      │
	#		├────────────────────────────────┤
	#		│ caller's third parameter       │
	#		├────────────────────────────────┤
	#		│ <previous item>                │
	#		├────────────────────────────────┤

	#	Body
	#	====

	#	Used Registers
	#	--------------

	#	I use the registers `r8`, `r10`, `r14`, `r12` and `r9`.  In order to
	#	restore these registers to their current condition when I finish, I
	#	push them all to the stack.

	push %r8
	push %r9
	push %r10
	push %r11
	push %r12

	#	The stack and the value of the base and stack pointer registers now
	#	look like

	#		┌────────────────────────────────┐
	#		│ value of `r12` before call     │ < stack pointer
	#		├────────────────────────────────┤
	#		│ value of `r11` before call     │
	#		├────────────────────────────────┤
	#		│ value of `r10` before call     │
	#		├────────────────────────────────┤
	#		│ value of `r9` before call      │
	#		├────────────────────────────────┤
	#		│ value of `r8` before call      │
	#		├────────────────────────────────┤
	#		│ base pointer before call       │ < base pointer
	#		├────────────────────────────────┤
	#		│ caller's return address        │
	#		├────────────────────────────────┤
	#		│ caller's first parameter       │
	#		├────────────────────────────────┤
	#		│ caller's second parameter      │
	#		├────────────────────────────────┤
	#		│ caller's third parameter       │
	#		├────────────────────────────────┤
	#		│ <previous item>                │
	#		├────────────────────────────────┤

	#	I copy the caller's first, second and third parameters (the items 16,
	#	24 and 32 bytes respectively beyond the base pointer) to the registers
	#	`rax`, `r8` and `r10` respectively.

	mov 16(%rbp), %rax
	mov 24(%rbp), %r8
	mov 32(%rbp), %r9

	#	Thus,
	#
	#	-   `rax` holds the content to be written to memory as a
	#	    null-terminated ASCII decimal string,
	#
	#	-   `r8` holds the memory address where the string should start, and
	#
	#	-   `r9` holds the maximum number of bytes, including the null
	#	     terminator, that may be written to the memory address.
	#
	#	If the number of bytes in the null-terminated string exceeds the value
	#	of `r9` then the leading bytes are ignored.

	################

	mov %r9, %r10

	#	I use the number in register `rcx` as an offset to traverse through the
	#	memory at the address in register `rbx from high to low.  I set the
	#	number in `rcx` to the highest possible offset by decrementing its
	#	current value, the length of the memory indicated by `rbx`.

	dec %r10

	#	I write the null character at the highest offset of the memory
	#	allocated to.

	movb $0, (%r8,%r10)

	#	I start a loop in which I write characters to the rest of the memory
	#	allocated to `pid` from highest offset to lowest.  Starting from the
	#	subroutine parameter `«num»`, I repeatedly divide `«num»` by `10`,
	#	writing the moduli to the string from the right until either the memory
	#	is full or `«num»` has been reduced to `0`.

	begin1:

		#	If the offset is zero then I have filled the memory.  I exit this
		#	loop.

		cmp $0, %r10
		jl end1

		dec %r10

		#	On the 64-bit Intel/AMD architevture, the instruction
		#
		#		div %«reg»
		#
		#	divides the 128 bit number whose high 64 bits are in register `rdx`
		#	and low 64 bits are in register `rax` by the value of register
		#	`«reg»`, and writes the quotient to `rax` and the remainder to
		#	`rdx`.  I write decimal 0 to `rdx`, write decimal `10` to register
		#	`r11`, and divide by `r11`.  Therefore, if the content of `rax` is
		#	`«num»`, then I wrote
		#
		#	    «num»/10
		#
		#	to `rax` and
		#
		#	    «num»%10
		#
		#	to `rdx`.

		mov $0, %rdx
		mov $10, %r11
		div %r11

		#	I write `«num»%10 + 48` to the current offset, and decrement the
		#	offset.  The addition of `48` converts the byte `«num»%` to the
		#	ASCII code for the corresponding decimal digit.

		movb %dl,(%r8,%r10)
		add $48, (%r8,%r10)

		#	If `«num»/10` is not `0` then I have converted the entirety of the
		#	subroutine parameter to ASCII decimal digits, so I repeat the loop
		#	with `«num»/10` in place of `«num»`,...

		cmp $0, %rax
		jne begin1
	end1:

	mov %r9,%rax
	sub %r10,%rax

	mov $0, %r11

	begin2:
		mov (%r8,%r10), %r12
		mov %r12, (%r8,%r11)	

		inc %r10
		inc %r11

		cmp %r9, %r10
		jne begin2

	begin3:
		movb $0, (%r8,%r11)

		inc %r11

		cmp %r9, %r11
		jne begin3

	pop %r12
	pop %r11
	pop %r10
	pop %r9
	pop %r8

	mov %rbp, %rsp
	pop %rbp

	ret
